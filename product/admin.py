from django.contrib import admin
from .models import Categories, Food

admin.site.register(Categories)
admin.site.register(Food)
