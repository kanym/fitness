from django.conf import settings
from django.db import models
from django.urls import reverse

 


class Categories(models.Model):

    title = models.CharField(max_length=50)
    body = models.TextField(verbose_name='содержимое страницы')
    image = models.ImageField(upload_to='BreakfastImg', null=True, blank=True)

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('detail', args=[str(self.id)])

    class Meta:
        verbose_name = "категория"
        verbose_name_plural = "Категории"


class Food(models.Model):
    categories = models.ForeignKey(Categories, on_delete=models.CASCADE, related_name='foods')
    title = models.TextField()
    body = models.TextField(verbose_name='содержимое страницы')
    weight = models.PositiveSmallIntegerField(verbose_name="вес", default=0)
    price = models.CharField(verbose_name='цена',  max_length=50)
    image = models.ImageField(verbose_name='фото', upload_to='MenuImg', null=True, blank=True)


    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('list') 

    class Meta:
        verbose_name = "меню"
        verbose_name_plural = "Меню "

