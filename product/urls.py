from django.urls import path
from .views import  CategoriesListView, CategoriesUpdateView, CategoriesDetailView, CategoriesDeleteView, CategoriesCreateView

urlpatterns = [
    path('<int:pk>/edit/', CategoriesUpdateView.as_view(), name='c_edit'), 
    path('<int:pk>/', CategoriesDetailView.as_view(), name='c_detail'), 
    path('<int:pk>/delete/', CategoriesDeleteView.as_view(), name='c_delete'), 
    path('create/', CategoriesCreateView.as_view(), name='c_create'),
    path('', CategoriesListView.as_view(), name='c_list'),
    # path('', CategoriesPageView.as_view(), name='categories'),
    
]