from django.shortcuts import render
from product.models import Categories
from django.views.generic import ListView, UpdateView, DetailView, DeleteView, CreateView
from django.views import generic
from django.urls import reverse_lazy
from django.contrib.auth.mixins import LoginRequiredMixin #если юзер авторизован то он даст возможность делать новый пост а если нет то перенаправляет на страницу догина
from django.core.exceptions import PermissionDenied #



# class CategoriesPageView(ListView):
#     model = Categories
#     context_object_name = 'categories'
#     template_name = 'base.html'


class CategoriesListView(ListView):
    model = Categories
    context_object_name = 'categories'
    template_name = 'base.html'


class CategoriesDetailView(DetailView):
    model = Categories
    template_name = 'categories/detail.html' 
     

class CategoriesDeleteView(LoginRequiredMixin, DeleteView):
    model = Categories
    template_name = 'categories/delete.html'
    success_url = reverse_lazy('home') 
    login_url = 'login' 

    def dispatch(self, request, *args, **kwargs):  #это то что мы делали в темплейтах 
        obj = self.get_object() 
        if obj.author != self.request.user:
            raise PermissionDenied
        return super().dispatch(request, *args, **kwargs)


class CategoriesUpdateView(LoginRequiredMixin, UpdateView):
    model = Categories
    template_name = 'categories/update.html'
    fields = ('title', 'body')
    success_url = reverse_lazy('home')
    login_url = 'login'

    def dispatch(self, request, *args, **kwargs):  #
        obj = self.get_object()
        if obj.author != self.request.user:
            raise PermissionDenied
        return super().dispatch(request, *args, **kwargs)


class CategoriesCreateView(LoginRequiredMixin, CreateView):
    model = Categories
    template_name = 'categories/create.html'
    fields = ('title', 'body')
    success_url = reverse_lazy('home')
    login_url = 'login'

    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)
