from django.db import models

class Reservation(models.Model):

    person = models.CharField(max_length=50, blank=True)
    date = models.DateTimeField(auto_now_add=True )
    time = models.TimeField(auto_now_add=True)



    def __str__(self):
        return self.person
    
