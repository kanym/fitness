from django.urls import path
from .views import MainListView, CreatePageView

urlpatterns = [
    path("", MainListView.as_view(), name='main'),
    path("create/", CreatePageView.as_view(), name='create'),
]