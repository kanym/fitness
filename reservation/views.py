from django.shortcuts import render
from django.views.generic import ListView, CreateView
from reservation.models import Reservation
from django.urls import reverse_lazy


class MainListView(ListView):
    model = Reservation
    template_name = 'reservation/main.html'

class CreatePageView(CreateView):
    model = Reservation
    template_name = 'reservation/create.html'
    fields = '__all__'
    success_url = reverse_lazy("home")