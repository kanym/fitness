from django.urls import path
from .views import SignUpView, PersonalView

urlpatterns = [
    path("users", SignUpView.as_view(), name="signup"),
    path("area/", PersonalView.as_view(), name="area"),
]