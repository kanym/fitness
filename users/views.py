from django.contrib.auth.forms import UserCreationForm
from django.views import generic
from django.urls import reverse_lazy
from django.views.generic import ListView
from product.models import Categories

class SignUpView(generic.CreateView):
    form_class = UserCreationForm
    success_url = reverse_lazy("login")
    template_name = "users/signup.html"


class PersonalView(ListView):
    model = Categories
    template_name = "users/area.html"

    